var Timer = function(io) {
    var self = this;
    this.availableStatuses = ['STOPPED', 'STARTED'];
    this.availableCountStatuses = ['FORWARD', 'BACK'];
    this.countStatus = 'BACK';
    this.status = 'STOPPED';
    this.settedTime = 0;
    this.value = 0;
    this.timeInterval = false;
    this.style = 'normal';

    this.start = function() {
        if(typeof this.timeInterval == 'undefined' || this.timeInterval === false) {
            io.sockets.emit('setInfo', this.simpleParseToDisplay(this.settedTime));
            this.timeInterval = setInterval(this.update, 1000);
        }
    };

    this.stop = function() {
        if(typeof this.timeInterval != 'undefined' || this.timeInterval != false) {
            clearInterval(this.timeInterval);
            this.timeInterval = false;
            io.sockets.emit('changeStyle', 'grey');
        }
    };

    this.set = function(value) {
        this.stop();
        this.settedTime = value;
        this.setValue(value);
        this.countStatus = 'BACK';
        this.style = 'normal';
        io.sockets.emit('set', this.getData());
    };

    this.plusMinus = function(value) {
        if (this.status == 'STOPPED') {
            this.set(value);
        } else {
            this.set(value, false);
        }
        this.set(value, false);
        if (this.status == 'STARTED') {
            this.start();
        }
    };

    this.setStatus = function(status) {
        if(this.availableStatuses.indexOf(status) !== -1){
            this.status = status;
        }
    };

    this.setSettedTime = function(value) {
        this.settedTime = value;
    };

    this.getSettedTime = function() {
        return this.settedTime;
    };

    this.setValue = function(value) {
        this.value = value;
    };

    this.getData = function () {
        return {
            value: this.parseToDisplay(this.value),
            rawValue: this.value,
            style: this.style
        };
    };

    this.simpleParseToDisplay = function(seconds) {
        var min = parseInt(seconds / 60, 10);
        var sec = parseInt(seconds % 60, 10);

        min = min < 10 ? "0" + min : min;
        sec = sec < 10 ? "0" + sec : sec;

        return min + ':' + sec;
    };

    this.parseToDisplay = function(seconds) {
        self.style = 'normal';

        if(seconds > 0 && seconds < 60) {
            self.style = 'warning';
        }
		
        if(seconds < 0) {
            seconds = Math.abs(seconds);
            self.countStatus = 'FORWARD';
            self.style = 'danger';
        } else {
            self.countStatus = 'BACK';
        }

        if(self.status == 'STOPPED') {
            self.style = 'grey';
        }

        var min = parseInt(seconds / 60, 10);
        var sec = parseInt(seconds % 60, 10);

        min = (self.status == "STOPPED" && min == 0 && sec == 0) ? "0" + min : min;
        sec = sec < 10 ? "0" + sec : sec;

        return (self.countStatus == 'FORWARD' && min < 10) ? "-" + min + ":" + sec : min + ":" + sec;
    };

    this.update = function() {
        var a = Math.abs(self.value);
        --self.value;

        // reset do 0 jesli przekracza 99:59
        if (a >= ((99 * 60) + 59)) {
            self.value = 0;
        }

        io.sockets.emit('update', self.getData());
    }
};

module.exports = Timer;