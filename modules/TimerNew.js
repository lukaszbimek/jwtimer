let moment = require('moment');
let Timer = class {
    constructor(io) {
        this.io = io;
    }
    initialValue = 0; // value of setted seconds
    secondsToDisplay = 0; // value of current seconds depends on counter type; if counter type is forward this value starts from 0 1 2 3, but if counter type is backward this value is 60 59 58// value of current seconds depends on counter type; if counter type is forward this value starts from 0 1 2 3, but if counter type is backward this value is 60 59 58
    remainingTime = 0; // value of current second to expiredDate
    expiredDate = moment();
    availableStatuses = ['STOPPED', 'STARTED'];
    currentStatus = 'STOPPED';
    availableCounterTypes = ['FORWARD', 'BACKWARD'];
    currentCounterType = 'BACKWARD';
    availableStyles = ['normal', 'warning', 'danger', 'grey'];
    style = 'normal';
    timeInterval = false;

    start() {
        if (this.timeInterval !== false) {
            return;
        }
        let expiredDate = moment();
        this.setExpiredDate(expiredDate.add(moment.duration(this.remainingTime, 'seconds')));
        this.io.sockets.emit('setInfo', this.parseToDisplay(this.getInitialValue()));
        this.setCurrentStatus('STARTED');
        this.timeInterval = setInterval(this.update.bind(this), 1000);
    }
    update() {
        this.calculateRemainingTime();
        this.calculateSecondsToDisplay();
        this.calculateStyle(this.remainingTime, this.getCurrentCounterType(), this.getInitialValue(), this.getCurrentStatus());
        this.io.sockets.emit('update', this.getDTO());
    }
    calculateSecondsToDisplay() {
        let currentDate = moment();
        var diffBackwards = this.getExpiredDate().diff(currentDate);
        var secondsTo = Math.round(moment(diffBackwards).valueOf() / 1000);
        if (this.getCurrentCounterType() === 'BACKWARD') {
            this.secondsToDisplay = secondsTo;
        }
        if (this.getCurrentCounterType() === 'FORWARD') {
            this.secondsToDisplay = this.getInitialValue() - secondsTo;
        }
    }
    stop() {
        if(this.timeInterval === false) {
            return;
        }
        this.setCurrentStatus('STOPPED');
        clearInterval(this.timeInterval);
        this.timeInterval = false;
        this.io.sockets.emit('changeStyle', 'grey');
    }
    set(value) {
        if(this.timeInterval !== false) {
            clearInterval(this.timeInterval);
            this.timeInterval = false;
        }
        var currentDate = moment();
        this.setExpiredDate(currentDate.add(moment.duration(value, 'seconds')));
        this.setCurrentStatus('STOPPED');
        this.initialValue = value;
        this.secondsToDisplay = value;
        this.remainingTime = value;
        this.setStyle('normal');
        this.io.sockets.emit(
            'set', 
            {
                value: this.parseToDisplay(value),
                rawValue: this.value,
                style: this.style
            }
        );
    }
    calculateRemainingTime() {
        var currentDate = moment();
        this.remainingTime = Math.round(this.getExpiredDate().diff(currentDate).valueOf() / 1000);
    }
    plus(value) {
        if (this.getCurrentStatus() === 'STOPPED') {
            this.set(this.secondsToDisplay + value);
            return;
        }
        if (this.getCurrentCounterType() === 'FORWARD') {
            this.setExpiredDate(this.getExpiredDate().subtract(moment.duration(value, 'seconds')));
        } 
        if (this.getCurrentCounterType() === 'BACKWARD') {
            this.setExpiredDate(this.getExpiredDate().add(moment.duration(value, 'seconds')));
        }
        this.calculateRemainingTime();
        this.io.sockets.emit('set', this.getDTO());
        if (this.status == 'STARTED') {
            this.start();
        }
    }
    minus(value) {
        if (this.getCurrentStatus() === 'STOPPED') {
            this.set(this.secondsToDisplay - value);
            return;
        }
        if (this.getCurrentCounterType() === 'FORWARD') {
            if (this.secondsToDisplay - value < 0) {
                value = this.secondsToDisplay;
            }
            this.setExpiredDate(this.getExpiredDate().add(moment.duration(value, 'seconds')));
        } 
        if (this.getCurrentCounterType() === 'BACKWARD') {
            this.setExpiredDate(this.getExpiredDate().subtract(moment.duration(value, 'seconds')));
        }
        this.calculateRemainingTime();
        this.io.sockets.emit('set', this.getDTO());
        if (this.status == 'STARTED') {
            this.start();
        }
    }
    getDTO() {
        return {
            value: this.parseToDisplay(this.secondsToDisplay),
            rawValue: this.getRemainingTime(),
            style: this.getStyle()
        }
    }
    parseToDisplay(value) {
        var prefix = '';
        var seconds = value;
        if(value < 0) {
            seconds = Math.abs(value);
            prefix = '-'
        }
        var min = parseInt(seconds / 60, 10);
        var sec = parseInt(seconds % 60, 10);

        min = min < 10 ? "0" + min : min;
        sec = sec < 10 ? "0" + sec : sec;

        return prefix + min + ':' + sec;
    }
    calculateStyle(remainingTime, counterType, initialValue, status) {
        this.setStyle('normal'); // initial value
        if (initialValue === 0) {
            return;
        }
        if (status === 'STOPPED') {
            this.setStyle('grey');
            return;
        }
        if (remainingTime < 60) {
            this.setStyle('warning');
        }
        if (remainingTime <= 0) {
            this.setStyle('danger');
        }
    }
    getInitialValue() {
        return this.initialValue;
    }
    getRemainingTime() {
        return this.remainingTime;
    }
    setRemainingTime(time) {
        this.remainingTime = time;
    }
    getExpiredDate() {
        return this.expiredDate;
    }
    setExpiredDate(date) {
        this.expiredDate = date;
    }
    getCurrentStatus() {
        return this.currentStatus;
    }
    setCurrentStatus(status) {
        if (this.availableStatuses.indexOf(status) === -1) {
            throw new Error('You try to set unavailable status: "'+ status +'"');
        }
        this.currentStatus = status;
    }
    getCurrentCounterType() {
        return this.currentCounterType;
    }
    setCurrentCounterType(type) {
        if (this.availableCounterTypes.indexOf(type) === -1) {
            throw new Error('You try to set unavailable counter type: "'+ type +'"');
        }
        this.currentCounterType = type;
    }
    getStyle() {
        return this.style;
    }
    setStyle(style) {
        if (this.availableStyles.indexOf(style) === -1) {
            throw new Error('You try to set unavailable style: "'+ style +'"');
        }
        this.style = style;
    }
}
 
module.exports = Timer;