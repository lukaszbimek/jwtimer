# JWTimer #

Aplikacja do odliczania czasu.

## 1. Wymagane oprogramowanie ###

* npm
* NodeJS

Powyższe oprogramoanie można pobrać ze strony `https://nodejs.org/en/download` dostępne jako jeden instalator.  

## 2. Uruchomienie serwera ###

#### 2.1. Umieszczenie plików na komputerze.
Przekopiować pliki do folderu np. `timer` (dowolna lokalizacja).
#### 2.2. Instalacja bibliotek.
Wejść do głownego folderu aplikacji np. `timer`. Uruchomić polecenie `npm install` które stworzy folder `node_modules` i pobierze wszystkie potrzebne do uruchomienia paczki.
#### 2.4. Uruchomienie serwera.
Uruchomić aplikację poleceniem:
```
node app.js
```

#### 2.5. Sprawdzenie działania aplikacji.
* Widok administracyjny (wykorzystywany w tablecie)
```
http://{ip}:3000/admin
```
* Widok wyświetlania czasu (wykorzystywany na komputerze gdzie jest serwer)
```
http://{ip}:3000/client
```

gdzie `{ip}` to adres IP komputera na którym została uruchomiona aplikacja.
Jeśli chcesz uruchomić na lokalnym komputerze, podmień `{ip}` na `127.0.0.1` lub `localhost`.

UWAGA! Najlepiej gdyby komputer na którym będzie uruchamiana aplikacja miał stały adres IP. W tym celu należy wprowadzić odpowiednią konfigurację w urządzeniu sieciowym.


## 3. Automatyzacja uruchomienia aplikacji ###
#### 3.1. Uruchamianie aplikacji jako usługa systemowa (system WINDOWS).
Przygotowany został mechanizm pozwalający zautomatyzować uruchamianie aplikacji: stworzenie usługi która będzie uruchamiać aplikację przy starcie systemu.

Aby zainstalować JWTimer jako usługę systemową należy:
###### 3.1.1. Edycja pliku `installServiceTimer.js`
Wyedytować plik `helpers/installServiceTimer.js` i podmienić linie numer 7:
```
script: 'C:\\timer\\app.js'
```
na prawidłową ścieżkę do pliku `app.js`.

###### 3.1.2. Uruchomienie skryptu tworzącego usługę.
Wejść do głównego folderu aplikacji np. `timer` i uruchomić polecenie (jako administrator):
```
node helpers/installServiceTimer.js
```
###### 3.1.3. Weryfikacja ustawień nowo stworzonej usługi.
Wejść w listę usług systemowych i sprawdzić czy znajduję się tam usługa zatytułowana: "jwtimer-server". Upewnić się czy usługa jest automatycznie uruchamiana przy starcie systemu.


#### 3.2. Automatyczne uruchamianie przeglądarki Chrome w trybie pełnoekranowym
Należy utworzyć nowy wpis w harmonogramie który uruchomi przeglądarkę w trybie pełnoekranowym oraz wyświetli widok wyświetlania czasu (pkt. 2.5)
Polecenie które uruchomi przeglądarke Chrome w trybie pełnoekranowym z uruchomionym widokiem wyświetlania czasu (pkt. 2.5):
```
...\chrome.exe" --kiosk http://{ip}:3000/client
```


#### 3.3. Ukrycie paska zadań (opcjonalnie)
Jeśli po uruchomieniu przeglądarki okaże się że pasek zadań jest widoczny, trzeba użyć zewnętrznego oprogramowania aby go ukryć. Na przykład: `http://www.itsamples.com/taskbar-hider.html`

