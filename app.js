const express = require('express');
const app = express();
const socket = require('socket.io');
const http = require('http');
const server = http.Server(app);

const routes = require('./routes/routes');

app.set('view engine', 'ejs');
app.locals.pretty = true; // pretty code source
app.use(routes);

// middleware
app.use('/public', express.static(__dirname + '/public'));
app.use('/js', express.static(__dirname + '/node_modules/socket.io/client-dist')); 
app.use('/js', express.static(__dirname + '/node_modules/bootstrap/dist/js'));
app.use('/js', express.static(__dirname + '/node_modules/jquery/dist')); 
app.use('/css', express.static(__dirname + '/node_modules/bootstrap/dist/css')); 

const io = socket(server);
const timerModule = require('./modules/TimerNew');
let Timer = new  timerModule(io);

io.on('connection', function (socket) {
    console.log('Client connected: ' + socket.id);
    socket.emit('connected', { id: socket.id, message: 'Welcome, connection established!'});

    socket.emit('update', Timer.getDTO());
    socket.emit('setCounterType', Timer.getCurrentCounterType());
    if (Timer.status === 'STARTED') {
        socket.emit('setInfo', Timer.parseToDisplay(Timer.getInitialValue()));
    }

    socket.on('timerSet', function(data) {
        Timer.setCurrentStatus('STOPPED');
        Timer.set(data);
    });

    socket.on('timerStart', function(data) {
        Timer.setCurrentStatus('STARTED');
        Timer.start();
    });

    socket.on('timerStop', function(data) {
        Timer.setCurrentStatus('STOPPED');
        Timer.stop();
    });

    socket.on('timerMinus', function(data) {
        Timer.minus(data);
    });

    socket.on('timerPlus', function(data) {
        Timer.plus(data);
    });

    socket.on('counterTypeChange', function(type) {
        Timer.setCurrentCounterType(type);
        io.sockets.emit('setCounterType', Timer.getCurrentCounterType());
    });
});

server.listen(3000);
console.log('Server running on port 3000');
