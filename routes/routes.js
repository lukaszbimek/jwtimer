const express = require('express');
const router = express.Router();

router.get('/', function(req, res){
    res.render('index');
});

router.get('/client', function(req, res){
    res.render('client');
});

router.get('/admin', function(req, res){
    res.render('admin');
});

module.exports = router;