$(function ($) {
    var socket = io.connect(timer.config.socketHost);
    var localTimer;
    var $timer = $(".timer");

    var calculateStyle = function (activeClass) {
        var classList = $timer.prop("classList");

        if (!$timer.hasClass(activeClass)) {
            $timer.addClass(activeClass);
            if(activeClass == 'grey') {
                $timer.addClass('blink_me');
            }
        }
        if ($timer.hasClass('danger') && activeClass != 'danger') {
            $timer.removeClass('danger')
        }

        if ($timer.hasClass('warning') && activeClass != 'warning') {
            $timer.removeClass('warning')
        }

        if ($timer.hasClass('grey') && activeClass != 'grey') {
            $timer.removeClass('grey');
            $timer.removeClass('blink_me');
        }

        if ($timer.hasClass('normal') && activeClass != 'normal') {
            $timer.removeClass('normal')
        }
    };

    var updateTimer = function (data) {
        calculateStyle(data.style);
        $timer.attr('data-raw', data.rawValue);
        $timer.text(data.value);
    };

    var updateClock = function () {
        var today = new Date();
        var h = today.getHours();
        var m = today.getMinutes();
        var s = today.getSeconds();
        m = checkTime(m);
        s = checkTime(s);
        $('.clockwrap').html(h + ":" + m + '<span>. ' + s + '</span>');
        var t = setTimeout(updateClock, 500);
    };

    var checkTime = function (i) {
        if (i < 10) {i = "0" + i};  // add zero in front of numbers < 10
        return i;
    };

    socket.on('update', function(data){
        localTimer = data.value;
        updateTimer(data);
    });

    socket.on('set', function(data){
        updateTimer(data);
    });

    socket.on('changeStyle', function(data) {
        calculateStyle(data);
    });

	socket.on('connected', function(data) {
        $('.offline').hide();
    });

	socket.on('disconnect', function () {
		$('.offline').show();
	});

    updateClock();
});
