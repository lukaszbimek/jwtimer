$(function ($) {
    var socket = io.connect(timer.config.socketHost);
    var startButton = $('.startTimer');
    var stopButton = $('.stopTimer');
    var plusMinusButton = $('.btn-live');

    var set = function (minutes, seconds) {
        minutes = isNaN(parseInt(minutes)) ? 0 : parseInt(minutes);
        seconds = isNaN(parseInt(seconds)) ? 0 : parseInt(seconds);
        $('.info').text('');
        socket.emit('timerSet', (minutes * 60) + seconds);
    };

    $(document).on('change', 'input:radio[name=counterType]', function () {
        var selectedType = $(document).find("input[name='counterType']:checked").val();
        socket.emit('counterTypeChange', selectedType);
    });

    $('.btn-time').on('click', function () {
        var minutes = $(this).data('minutes');
        var seconds = $(this).data('seconds');
        set(minutes, seconds);
    });

    startButton.on('click', function (e) {
        e.preventDefault();
        socket.emit('timerStart');
        $('.timer').removeClass('blink_me');
    });

    stopButton.on('click', function (e) {
        e.preventDefault();
        socket.emit('timerStop');
    });

    plusMinusButton.on('click', function () {
        var type = $(this).data('change');
        switch (type) {
            case 'plus':
                socket.emit('timerPlus', 60);
                break;

            case 'minus':
                socket.emit('timerMinus', 60);
                break;
        }
    });

    socket.on('setInfo', function (info) {
        $('.info').html('Czas punktu: <span>' + info + '</span>');
    });

    socket.on('setCounterType', function(type) {
        $('input:radio[name=counterType]').prop('checked', false);
        $(document).find('input:radio[value='+type+']').prop('checked', true);
    });

    socket.on('connectClient', function (data) {
        console.log(data);
    });

    socket.on('connected', function(data){
        $('.adminoffline').hide();
    });

    socket.on('disconnect', function () {
        $('.adminoffline').show();
    });
});
