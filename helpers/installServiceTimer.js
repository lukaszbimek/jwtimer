var Service = require('node-windows').Service;
 
// Create a new service object
var svc = new Service({
  name:'jwtimer-server',
  description: 'The websocket server for the Timer.',
  script: 'C:\\timer\\app.js'
});
 
// Listen for the 'install' event, which indicates the
// process is available as a service.
svc.on('install',function(){
  svc.start();
});
 
// install the service
svc.install();